import 'package:flutter_web_auth/flutter_web_auth.dart';
import 'package:logger/logger.dart';
import 'package:lotr/commons/models/InformationPost.dart';
import 'package:lotr/commons/models/RedditList.dart';
import 'package:lotr/commons/models/RedditThing.dart';
import 'package:reddit/reddit.dart';
import 'package:http/http.dart';

//Information about reddit
const clientId = 'O41Y6sf4LeoqWhXDYrfd0g';
const callbackUrlScheme = 'lotr-naylu';
const redirectUri = '$callbackUrlScheme://callback';

class RedditService {
  static final _instance = RedditService._();
  factory RedditService() {
    return _instance;
  }
  RedditService._() {
    _reddit.authSetup(clientId, "");
  }

  final logger = Logger();
  final _reddit = new Reddit(Client());
  Uri _authUrl;

  Future<void> authenticate() async {
    final state = 'STATE_RANDOM';

    if (_authUrl == null) {
      _authUrl = _reddit.authUrl(
        redirectUri,
        state: state,
        scopes: ['identity', 'read', 'vote'],
      );
    }

    final result = await FlutterWebAuth.authenticate(
      url: _authUrl.toString(),
      callbackUrlScheme: callbackUrlScheme,
    );

    final response = Uri.parse(result).queryParameters;

    final responseState = response['state'];

    if (responseState != state) {
      throw Exception('corrupted state');
    }

    final code = response['code'];
    await _reddit.authFinish(code: code);
  }

  Future<List<InformationPost>> searchLotrPost(String term) async {
    final listingResult = await _reddit
        .sub('lotr')
        .search(term)
        .filter('count', 100)
        .filter('limit', 100)
        .fetch();

    final resultThing =
        RedditThing.fromJson(Map.fromEntries(listingResult.entries));

    final list = RedditList.fromJson(resultThing.data);
    return list.children.map((e) => InformationPost.fromJson(e.data)).toList();
  }

  Future<List<InformationPost>> getLotrPost() async {
    final listingResult =
        await _reddit.sub('lotr').newPosts().count(100).limit(100).fetch();

    final resultThing =
        RedditThing.fromJson(Map.fromEntries(listingResult.entries));

    final list = RedditList.fromJson(resultThing.data);
    return list.children.map((e) => InformationPost.fromJson(e.data)).toList();
  }
}
