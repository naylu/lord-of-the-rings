class Hobbit {
  Hobbit({this.name, this.image});

  final String name;
  final String image;
}
