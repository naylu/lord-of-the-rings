class RedditThing {
  RedditThing({
    this.id,
    this.name,
    this.data,
  });

  String id;
  String name;
  Map<String, dynamic> data;

  RedditThing copyWith({
    String id,
    String name,
    Map<String, dynamic> data,
  }) =>
      RedditThing(
        id: id ?? this.id,
        name: name ?? this.name,
        data: data ?? this.data,
      );

  factory RedditThing.fromJson(Map<dynamic, dynamic> json) => RedditThing(
        id: json["id"],
        name: json["name"],
        data: json["data"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "data": data,
      };
}
