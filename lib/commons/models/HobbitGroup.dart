import 'package:lotr/commons/models/InformationPost.dart';

class HobbitGroup {
  HobbitGroup({this.hobbit, this.posts = const []});

  final String hobbit;
  final List<InformationPost> posts;

  Map<String, dynamic> toJson() => {
        "hobbit": hobbit,
        "posts": List.from(posts.map((e) => e.toJson())),
      };
}
