enum AsyncStateStatus { idle, loading, success, error }

class AsyncState<T> {
  final AsyncStateStatus status;
  final T data;
  final dynamic error;

  bool get isIdle => status == AsyncStateStatus.idle;
  bool get isLoading => status == AsyncStateStatus.loading;
  bool get hasError => status == AsyncStateStatus.error;
  bool get hasData => status == AsyncStateStatus.success;

  AsyncState({
    this.status = AsyncStateStatus.idle,
    this.data,
    this.error,
  });

  factory AsyncState.loading() {
    return AsyncState(
        status: AsyncStateStatus.loading, data: null, error: null);
  }

  factory AsyncState.success(T data) {
    return AsyncState(
        status: AsyncStateStatus.success, data: data, error: null);
  }

  factory AsyncState.error(dynamic error) {
    return AsyncState(status: AsyncStateStatus.error, data: null, error: error);
  }
}
