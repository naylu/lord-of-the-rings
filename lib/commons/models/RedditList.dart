import 'package:lotr/commons/models/RedditThing.dart';

class RedditList {
  RedditList({
    this.after,
    this.before,
    this.modhash,
    this.children,
  });

  String after;
  String before;
  String modhash;
  List<RedditThing> children;

  RedditList copyWith({
    String after,
    String before,
    String modhash,
    List<RedditThing> children,
  }) =>
      RedditList(
        after: after ?? this.after,
        before: before ?? this.before,
        modhash: modhash ?? this.modhash,
        children: children ?? this.children,
      );

  factory RedditList.fromJson(Map<dynamic, dynamic> json) => RedditList(
        after: json["after"],
        before: json["before"],
        modhash: json["modhash"],
        children: List<RedditThing>.from(
            json["children"].map((x) => RedditThing.fromJson(x))),
      );

  Map<dynamic, dynamic> toJson() => {
        "after": after,
        "before": before,
        "modhash": modhash,
        "children": List<RedditThing>.from(children.map((x) => x.toJson())),
      };
}
