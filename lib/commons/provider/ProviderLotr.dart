import 'package:flutter/cupertino.dart';
import 'package:lotr/commons/models/AsyncState.dart';
import 'package:lotr/commons/models/HobbitGroup.dart';
import 'package:lotr/commons/models/InformationPost.dart';
import 'package:lotr/commons/reddit/RedditService.dart';
import 'package:provider/provider.dart';
import 'package:logger/logger.dart';

class ProviderLotr extends ChangeNotifier {
  static ProviderLotr of(BuildContext context, [bool listen = true]) {
    return Provider.of<ProviderLotr>(context, listen: listen);
  }

  var _listState = AsyncState<List<InformationPost>>();
  var _groupListState = AsyncState<List<HobbitGroup>>();
  final _reddit = RedditService();
  final logger = Logger();

  String hobbitSelected = "";
  List<HobbitGroup> get groupPostList => _groupListState.data ?? [];
  List<InformationPost> get postList => _listState.data ?? [];
  bool get isLoading => _listState.isLoading;
  bool get isLoadingGroup => _groupListState.isLoading;

  void setHobbit(String hobbit) {
    hobbitSelected = hobbit;
    hobbit.replaceAll("/", " ");
    getList(search: hobbit);
  }

  void clearHobbit({bool updateInformation = true}) {
    hobbitSelected = "";
    notifyListeners();
    if (updateInformation) {
      getList();
    }
  }

  void getList({String search}) async {
    _listState = AsyncState.loading();
    notifyListeners();
    try {
      List<InformationPost> list;
      if (search == null || search.isEmpty) {
        //Get first 100 post
        list = await _reddit.getLotrPost();
      } else {
        //Search post
        list = await _reddit.searchLotrPost(search);
      }
      _listState = AsyncState.success(list);
    } catch (e) {
      _listState = AsyncState.error("Algo salio mal. Intentalo de nuevo");
    }
    notifyListeners();
  }

  void groupPost() {
    _groupListState = AsyncState.loading();
    List<InformationPost> frodoPost = [];
    List<InformationPost> bilboPost = [];
    List<InformationPost> samPost = [];
    List<InformationPost> merryPost = [];
    List<InformationPost> pippinPost = [];
    List<InformationPost> gollumPost = [];
    List<HobbitGroup> list = [];

    //Separate post by hobbit
    postList.forEach((element) {
      if (element.title.contains("Frodo")) {
        frodoPost.add(element);
      }
      if (element.title.contains("Bilbo")) {
        bilboPost.add(element);
      }
      if (element.title.contains("Sam") || element.title.contains("Samsagaz")) {
        samPost.add(element);
      }
      if (element.title.contains("Meriadoc") ||
          element.title.contains("Merry")) {
        merryPost.add(element);
      }
      if (element.title.contains("Perigrin") ||
          element.title.contains("Pippin")) {
        pippinPost.add(element);
      }
      if (element.title.contains("Smeagol") ||
          element.title.contains("Gollum")) {
        gollumPost.add(element);
      }
    });

    //Order post with score and tittle
    frodoPost.sort((a, b) {
      int cmp = b.score.compareTo(a.score);
      if (cmp != 0) return cmp;
      return a.title.compareTo(b.title);
    });
    bilboPost.sort((a, b) {
      int cmp = b.score.compareTo(a.score);
      if (cmp != 0) return cmp;
      return a.title.compareTo(b.title);
    });
    samPost.sort((a, b) {
      int cmp = b.score.compareTo(a.score);
      if (cmp != 0) return cmp;
      return a.title.compareTo(b.title);
    });
    merryPost.sort((a, b) {
      int cmp = b.score.compareTo(a.score);
      if (cmp != 0) return cmp;
      return a.title.compareTo(b.title);
    });
    pippinPost.sort((a, b) {
      int cmp = b.score.compareTo(a.score);
      if (cmp != 0) return cmp;
      return a.title.compareTo(b.title);
    });
    gollumPost.sort((a, b) {
      int cmp = b.score.compareTo(a.score);
      if (cmp != 0) return cmp;
      return a.title.compareTo(b.title);
    });

    //Up-vote post
    if (frodoPost.isNotEmpty) {
      frodoPost.first.score = frodoPost.first.score + 1;
      frodoPost.first.clicked = true;
    }
    if (bilboPost.isNotEmpty) {
      bilboPost.first.score = bilboPost.first.score + 1;
      bilboPost.first.clicked = true;
    }
    if (samPost.isNotEmpty) {
      samPost.first.score = samPost.first.score + 1;
      samPost.first.clicked = true;
    }
    if (merryPost.isNotEmpty) {
      merryPost.first.score = merryPost.first.score + 1;
      merryPost.first.clicked = true;
    }
    if (pippinPost.isNotEmpty) {
      pippinPost.first.score = pippinPost.first.score + 1;
      pippinPost.first.clicked = true;
    }
    if (gollumPost.isNotEmpty) {
      gollumPost.first.score = gollumPost.first.score + 1;
      gollumPost.first.clicked = true;
    }

    //Add all post in generic model
    list = [
      HobbitGroup(hobbit: "Frodo", posts: frodoPost),
      HobbitGroup(hobbit: "Bilbo", posts: bilboPost),
      HobbitGroup(hobbit: "Sam/Samsagaz", posts: samPost),
      HobbitGroup(hobbit: "Meriadoc/Merry", posts: merryPost),
      HobbitGroup(hobbit: "Perigrin/Pippin", posts: pippinPost),
      HobbitGroup(hobbit: "Smeagol/Gollum", posts: gollumPost),
    ];
    _groupListState = AsyncState.success(list);
    notifyListeners();

    logger.d(List.from(list.map((e) => e.toJson())));
  }
}
