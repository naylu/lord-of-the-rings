import 'package:flutter/cupertino.dart';

class AppColors {
  static const Color orangeReddit = const Color(0xffFF571E);
  static const Color gold = const Color(0xffFFD700);
  static const Color blue = const Color(0xff003F6D);
}
