import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class InputText extends StatefulWidget {
  InputText({
    this.hintText = "",
    this.suffixIcon,
    this.controller,
    this.onSubmitted,
  });

  final String hintText;
  final Widget suffixIcon;
  final TextEditingController controller;
  final ValueChanged<String> onSubmitted;

  @override
  _InputTextState createState() => _InputTextState();
}

class _InputTextState extends State<InputText> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      padding: EdgeInsets.only(left: 10),
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(color: Colors.grey, width: 1.0),
        borderRadius: BorderRadius.all(Radius.circular(10.0)),
      ),
      child: TextFormField(
        decoration: InputDecoration(
          hintText: widget.hintText,
          hintStyle: TextStyle(color: Colors.grey),
          border: InputBorder.none,
          suffixIcon: widget.suffixIcon,
        ),
        controller: widget.controller,
        style: TextStyle(
          color: Colors.grey,
        ),
        onFieldSubmitted: widget.onSubmitted,
      ),
    );
  }
}
