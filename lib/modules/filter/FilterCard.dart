import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lotr/commons/models/Hobbit.dart';
import 'package:lotr/commons/provider/ProviderLotr.dart';
import 'package:lotr/commons/ui/AppColors.dart';
import 'package:provider/provider.dart';

class FilterCard extends StatefulWidget {
  FilterCard({Key key, this.hobbit}) : super(key: key);

  final Hobbit hobbit;
  @override
  _FilterCardState createState() => _FilterCardState();
}

class _FilterCardState extends State<FilterCard> {
  ProviderLotr provider;

  @override
  Widget build(BuildContext context) {
    provider = Provider.of<ProviderLotr>(context);

    return InkWell(
      onTap: () {
        provider.setHobbit(widget.hobbit.name);
        Navigator.pop(context);
      },
      child: Stack(children: [
        Container(
          margin: EdgeInsets.all(10),
          padding: EdgeInsets.all(15),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(5)),
              image: DecorationImage(
                  fit: BoxFit.fill, image: AssetImage(widget.hobbit.image))),
        ),
        Positioned(
          child: Container(
              margin: EdgeInsets.all(10),
              padding: EdgeInsets.all(15),
              decoration: BoxDecoration(
                gradient: RadialGradient(radius: 1, colors: [
                  AppColors.blue.withOpacity(0.4),
                  Colors.transparent
                ]),
                border: provider.hobbitSelected == widget.hobbit.name
                    ? Border.all(color: AppColors.gold, width: 3)
                    : null,
                borderRadius: BorderRadius.all(Radius.circular(5)),
              ),
              child: Center(
                child: Text(
                  widget.hobbit.name,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                  textAlign: TextAlign.center,
                ),
              )),
        ),
      ]),
    );
  }
}
