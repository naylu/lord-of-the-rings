import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lotr/commons/models/Hobbit.dart';
import 'package:lotr/commons/ui/Images.dart';
import 'package:lotr/modules/filter/FilterCard.dart';

class FilterPage extends StatefulWidget {
  FilterPage({Key key}) : super(key: key);
  @override
  _FilterPageState createState() => _FilterPageState();
}

class _FilterPageState extends State<FilterPage> {
  List<Hobbit> hobbit = [
    Hobbit(name: "Frodo", image: frodo),
    Hobbit(name: "Bilbo", image: bilbo),
    Hobbit(name: "Sam/Samsagaz", image: sam),
    Hobbit(name: "Merry/Meriadoc", image: meriadoc),
    Hobbit(name: "Pippin/Peregrin", image: pippin),
    Hobbit(name: "Smeagol/Gollum", image: gollum),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(
                height: 20,
              ),
              Center(
                  child: Text(
                "Selecciona un Hobbit",
                style: TextStyle(fontSize: 20),
              )),
              SizedBox(
                height: 10,
              ),
              GridView.builder(
                itemCount: hobbit.length,
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  childAspectRatio: 1,
                ),
                itemBuilder: (context, index) {
                  return FilterCard(hobbit: hobbit.elementAt(index));
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
