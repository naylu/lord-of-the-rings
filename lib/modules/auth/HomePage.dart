import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lotr/commons/models/AsyncState.dart';
import 'package:lotr/commons/reddit/RedditService.dart';
import 'package:lotr/commons/ui/AppColors.dart';
import 'package:lotr/commons/ui/Images.dart';
import 'package:lotr/modules/post/PostPage.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final _redditService = RedditService();
  var _loginState = AsyncState();

  void _onLogin() async {
    try {
      setState(() {
        _loginState = AsyncState.loading();
      });
      await _redditService.authenticate();
      Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(
          builder: (ctx) => PostPage(),
        ),
        ModalRoute.withName('/'),
      );
      setState(() {
        _loginState = AsyncState.success(null);
      });
    } catch (e) {
      setState(() {
        _loginState = AsyncState.error("Algo salio mal. Intenta de nuevo");
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
                image:
                    DecorationImage(fit: BoxFit.fill, image: AssetImage(home))),
          ),
          Positioned(
            child: Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.bottomRight,
                    end: Alignment.topRight,
                    colors: [
                      Colors.black.withOpacity(0.5),
                      Colors.transparent
                    ]),
              ),
              height: MediaQuery.of(context).size.height,
              child: Center(
                // ignore: deprecated_member_use
                child: RaisedButton(
                    color: AppColors.orangeReddit,
                    child: _loginState.isLoading
                        ? CircularProgressIndicator()
                        : Container(
                            width: 120,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Image.asset(
                                  logoReddit,
                                  height: 25,
                                  width: 25,
                                ),
                                Text(
                                  "Iniciar sesión",
                                  style: TextStyle(color: Colors.white),
                                )
                              ],
                            ),
                          ),
                    onPressed: _onLogin),
                //Image.asset(logoWhite)
              ),
            ),
          ),
        ],
      ),
    );
  }
}
