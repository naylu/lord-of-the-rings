import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lotr/commons/models/InformationPost.dart';
import 'package:lotr/commons/ui/AppColors.dart';

class PostCard extends StatelessWidget {
  PostCard({Key key, this.information}) : super(key: key);

  final InformationPost information;

  @override
  Widget build(BuildContext context) {
    return Card(
      shadowColor: AppColors.gold,
      elevation: 2.0,
      child: Container(
        width: MediaQuery.of(context).size.width - 40,
        margin: EdgeInsets.symmetric(
          vertical: 5,
        ),
        padding: EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  width: MediaQuery.of(context).size.width / 2 - 20,
                  child: Text(
                    "Publicado por " + information.author,
                    style: TextStyle(
                      color: Colors.grey,
                    ),
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                  ),
                ),
                information.linkFlairText != null
                    ? Container(
                        padding: EdgeInsets.all(5),
                        decoration: BoxDecoration(
                          color: AppColors.blue,
                          border: Border.all(
                            color: AppColors.blue,
                          ),
                          borderRadius: BorderRadius.all(Radius.circular(50)),
                        ),
                        child: Text(
                          information.linkFlairText,
                          style: TextStyle(color: Colors.white),
                          overflow: TextOverflow.ellipsis,
                        ),
                      )
                    : SizedBox()
              ],
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              information.title,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.black,
              ),
              maxLines: 3,
              overflow: TextOverflow.ellipsis,
            ),
            SizedBox(
              height: 5,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  information.numComments.toString() + " comentarios",
                  style: TextStyle(color: Colors.grey),
                ),
                Row(
                  children: [
                    IconButton(
                        icon: Icon(
                          Icons.keyboard_arrow_up,
                          color: information.clicked
                              ? AppColors.blue
                              : Colors.grey,
                        ),
                        onPressed: () {}),
                    Text(information.score.toString()),
                    IconButton(
                        icon: Icon(
                          Icons.keyboard_arrow_down,
                          color: Colors.grey,
                        ),
                        onPressed: () {}),
                  ],
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
