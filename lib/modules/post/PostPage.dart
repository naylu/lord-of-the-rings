import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lotr/commons/provider/ProviderLotr.dart';
import 'package:lotr/commons/ui/AppColors.dart';
import 'package:lotr/commons/ui/InputText.dart';
import 'package:lotr/modules/filter/FilterPage.dart';
import 'package:lotr/modules/post/GroupPost.dart';
import 'package:lotr/modules/post/PostCard.dart';
import 'package:provider/provider.dart';

class PostPage extends StatefulWidget {
  PostPage({Key key}) : super(key: key);
  @override
  _PostPageState createState() => _PostPageState();
}

class _PostPageState extends State<PostPage> {
  final searchController = TextEditingController();
  ProviderLotr provider;
  bool _isGroupPost = false;

  @override
  void initState() {
    provider = Provider.of<ProviderLotr>(context, listen: false);
    provider.getList();
  }

  @override
  Widget build(BuildContext context) {
    provider = Provider.of<ProviderLotr>(context);

    return Scaffold(
      backgroundColor: Colors.grey[50],
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(
                height: 10,
              ),
              Text(
                "Señor de los anillos",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 26,
                    color: AppColors.blue),
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 20),
                child: Column(
                  children: [
                    Divider(
                      thickness: 2,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          width: 250,
                          child: InputText(
                            hintText: "Búsqueda",
                            controller: searchController,
                            onSubmitted: (value) {
                              _isGroupPost = false;
                              provider.clearHobbit(updateInformation: false);
                              provider.getList(search: value);
                            },
                            suffixIcon: IconButton(
                              onPressed: () {
                                _isGroupPost = false;
                                provider.clearHobbit(updateInformation: false);
                                provider.getList(search: searchController.text);
                              },
                              icon: Icon(
                                Icons.search,
                                color: Colors.grey,
                              ),
                            ),
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            setState(() {
                              _isGroupPost = !_isGroupPost;
                            });
                          },
                          child: Icon(
                            Icons.group_work_sharp,
                            color: _isGroupPost ? AppColors.blue : Colors.grey,
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            searchController.text = "";
                            _isGroupPost = false;
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => FilterPage()));
                          },
                          child: Icon(
                            Icons.filter_alt,
                            color: Colors.grey,
                          ),
                        ),
                      ],
                    ),
                    provider.hobbitSelected != ""
                        ? Row(
                            children: [
                              Text(
                                "Filtrado por: " + provider.hobbitSelected,
                                style: TextStyle(color: Colors.grey),
                              ),
                              IconButton(
                                  icon: Icon(
                                    Icons.clear,
                                    size: 14,
                                    color: Colors.grey,
                                  ),
                                  onPressed: () {
                                    provider.clearHobbit();
                                  })
                            ],
                          )
                        : SizedBox(),
                    _isGroupPost
                        ? GroupPost()
                        : provider.isLoading
                            ? Center(
                                child: CircularProgressIndicator(
                                  backgroundColor: AppColors.blue,
                                ),
                              )
                            : Column(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  Text(
                                      "Total: " +
                                          provider.postList.length.toString(),
                                      style: TextStyle(color: Colors.grey)),
                                  Column(
                                    children: provider.postList
                                        .map((e) => PostCard(
                                              information: e,
                                            ))
                                        .toList(),
                                  ),
                                ],
                              ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
