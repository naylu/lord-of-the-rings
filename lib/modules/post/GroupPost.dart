import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lotr/commons/models/HobbitGroup.dart';
import 'package:lotr/commons/provider/ProviderLotr.dart';
import 'package:lotr/commons/ui/AppColors.dart';
import 'package:lotr/modules/post/PostCard.dart';
import 'package:provider/provider.dart';

class GroupPost extends StatefulWidget {
  GroupPost({Key key}) : super(key: key);
  @override
  _GroupPostState createState() => _GroupPostState();
}

class _GroupPostState extends State<GroupPost> {
  ProviderLotr provider;

  @override
  void initState() {
    super.initState();
    provider = Provider.of<ProviderLotr>(context, listen: false);
    provider.groupPost();
  }

  Widget buildGroupPost() {
    provider = Provider.of<ProviderLotr>(context, listen: false);
    return Column(
        children: provider.groupPostList
            .map((hobbitGroup) => _buildGroup(hobbitGroup))
            .toList());
  }

  Widget _buildGroup(HobbitGroup hobbitGroup) {
    if (hobbitGroup.posts.isEmpty) {
      return SizedBox();
    }
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: 15,
        ),
        Text(
          hobbitGroup.hobbit,
          style: TextStyle(color: AppColors.blue),
        ),
        SizedBox(
          height: 10,
        ),
        Column(
          children: hobbitGroup.posts
              .map((e) => PostCard(
                    information: e,
                  ))
              .toList(),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    provider = Provider.of<ProviderLotr>(context);
    if (provider.isLoadingGroup) {
      return Center(
        child: CircularProgressIndicator(
          backgroundColor: AppColors.blue,
        ),
      );
    }
    return buildGroupPost();
  }
}
